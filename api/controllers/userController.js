'use strict';


const mongoose = require('mongoose'),

User = mongoose.model('User');

exports.listAllUsers = (req, res) => {

  User.find({}, (err, user) => {

    (err) ? res.send(err) : res.json(user);

  });
};

exports.createUser = (req, res) => {

  const newUser = new User(req.body);

  newUser.save( (err, user) => {
    (err) ? res.send(err) : res.json(user);
  });

};

exports.getUserById = (req, res) => {

  User.findById(req.params.userId, (err, user) => {

    (err) ? res.send(err) : res.json(user);
    
  }); 
};


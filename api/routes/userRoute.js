'use strict';

module.exports = function(app){

    const user = require('../controllers/userController');

    app.route('/user')
        .get(user.listAllUsers)
        .post(user.createUser);

    app.route('/user/:userId')
        .get(user.getUserById);
}


 const express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;
  mongoose = require('mongoose'),
  User = require('./api/models/userModel'),
  bodyParser = require('body-parser');


mongoose.Promise = global.Promise;
mongoose.connect('mongodb://cosmicode:CosmiCode2019@ds161074.mlab.com:61074/gamingpartnersdb', { useNewUrlParser: true });

app.use(bodyParser.urlencoded({ extended : true}));
app.use(bodyParser.json());

let routes = require('./api/routes/userRoute')
routes(app);

app.listen(port);

console.log('RESTful API server started on: ' + port);